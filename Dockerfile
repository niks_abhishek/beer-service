FROM openjdk:17-jdk-slim
COPY target/beer-service-1.0.0-RELEASE.jar beer-service-1.0.0-RELEASE.jar
CMD java -jar beer-service-1.0.0-RELEASE.jar
