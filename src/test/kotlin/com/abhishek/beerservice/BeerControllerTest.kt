package com.abhishek.beerservice

import io.mockk.clearMocks
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup
import io.restassured.module.mockmvc.kotlin.extensions.Extract
import io.restassured.module.mockmvc.kotlin.extensions.Given
import io.restassured.module.mockmvc.kotlin.extensions.Then
import io.restassured.module.mockmvc.kotlin.extensions.When
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

@ExtendWith(MockKExtension::class)
class BeerControllerTest {

    private val restTemplate: RestTemplate = mockk()

    @BeforeEach
    fun setup() {
        clearMocks(restTemplate)

        val beerController = BeerController(restTemplate)
        standaloneSetup(beerController)
    }

    @Test
    fun `can get beer only`() {

        Given {
            header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
        } When {
            get("/beers")
        } Then {
            statusCode(200)
            body("message", Matchers.equalTo("Cheers! from Beer Service"))
        }
    }

    @Test
    fun `can get beer with pizza`() {

        // mock Pizza Service call
        every {
            restTemplate.getForEntity("http://localhost:5050/pizza-service/pizzas", String::class.java)
        } returns
                ResponseEntity("Hell yeah!! Its pizza with beer", HttpStatus.OK)


        Given {
            header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
        } When {
            get("/beers-with-pizza")
        } Then {
            statusCode(200)
            body("message", Matchers.equalTo("Hell yeah!! Its pizza with beer"))
        } Extract {
            response().body.prettyPrint()
        }
    }
}
