package com.abhishek.beerservice

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter
import io.restassured.RestAssured
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BeerControllerIT(@LocalServerPort val port: String) {

    private val validationFilter = OpenApiValidationFilter("specification/beer-service-api-spec.json")

    @BeforeEach
    fun setup() {
        RestAssured.reset()
        RestAssured.port = port.toInt()
    }

    @Test
    fun `can call pizza from beer service`() {
        Given {
            header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
            filter(validationFilter)
        } When  {
            get("/beer-service/beers-with-pizza")
        } Then  {
            statusCode(200)
            body("message", Matchers.equalTo("Hell yeah!! Its pizza with beer"))
        }
    }
}
