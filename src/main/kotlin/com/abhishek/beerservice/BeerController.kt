package com.abhishek.beerservice

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@RestController
class BeerController(val restTemplate: RestTemplate) {

    @GetMapping("/beers")
    fun getBeerGreeting(): ResponseEntity<BeerResponse> {
        val beerResponse = BeerResponse(200, "Cheers! from Beer Service")
        return ResponseEntity.ok(beerResponse)
    }

    @GetMapping("/beers-with-pizza", produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun getBeerWithPizza(): ResponseEntity<BeerResponse> {

        val pizzaResponse = restTemplate
//            .getForEntity("http://pizza-service-wiremock:5050/pizza-service/pizzas", String::class.java)
            .getForEntity("http://localhost:5050/pizza-service/pizzas", String::class.java)
        println(pizzaResponse)

        val beerResponse = BeerResponse(200, "Hell yeah!! Its pizza with beer")
        return ResponseEntity.ok(beerResponse)
    }
}
