package com.abhishek.beerservice

data class BeerResponse(
    val status: Int,
    val message: String
)
